#include <webots/Robot.hpp>
#include <webots/DistanceSensor.hpp>
#include <webots/Motor.hpp>

// time in [ms] of a simulation step
#define TIME_STEP 64

#define MAX_SPEED 6.28

// All the webots classes are defined in the "webots" namespace
using namespace webots;


//--------------------------------------------------------------
//                    Variables declaration
//--------------------------------------------------------------

// Create the Robot instance.
Robot *robot = new Robot();

// Create the motors instance
Motor *leftMotor = robot->getMotor("left wheel motor");
Motor *rightMotor = robot->getMotor("right wheel motor");

// Create the sensors arrays
DistanceSensor *ps[8];
char psNames[8][4] = {
  "ps0", "ps1", "ps2", "ps3",
  "ps4", "ps5", "ps6", "ps7"
};
double psValues[8];

// Create the whell speed variables
double leftSpeed;
double rightSpeed;


//--------------------------------------------------------------
//                    Utility functions
//--------------------------------------------------------------

void init(){
  // initialize devices
  for (int i = 0; i < 8; i++) {
    ps[i] = robot->getDistanceSensor(psNames[i]);
    ps[i]->enable(TIME_STEP);
  }

  leftMotor->setPosition(INFINITY);
  rightMotor->setPosition(INFINITY);
  leftMotor->setVelocity(0.0);
  rightMotor->setVelocity(0.0);
}

void drive_forward(){
  // set both motors to full speed
  leftSpeed = MAX_SPEED;
  rightSpeed = MAX_SPEED;
  // write actuators inputs
  leftMotor->setVelocity(leftSpeed);
  rightMotor->setVelocity(rightSpeed);
}

void turn_left(){
  // turn left by setting opposite speeds
  leftSpeed  = -MAX_SPEED;
  rightSpeed = MAX_SPEED;
  // write actuators inputs
  leftMotor->setVelocity(leftSpeed);
  rightMotor->setVelocity(rightSpeed);
}

void turn_right(){
  // turn right by setting opposite speeds
  leftSpeed  = MAX_SPEED;
  rightSpeed = -MAX_SPEED;
  // write actuators inputs
  leftMotor->setVelocity(leftSpeed);
  rightMotor->setVelocity(rightSpeed);
}


//--------------------------------------------------------------
//                    Main function
//--------------------------------------------------------------

int main(int argc, char **argv) {
  // Initialize all the objects needed
  init();
  // feedback loop: step simulation until an exit event is received
  while (robot->step(TIME_STEP) != -1) {
    // read sensors outputs
    for (int i = 0; i < 8 ; i++)
      psValues[i] = ps[i]->getValue();
    // detect obstacles
    bool right_obstacle =
      psValues[1] > 90.0 ||
      psValues[2] > 90.0;
    bool left_obstacle =
      psValues[5] > 90.0 ||
      psValues[6] > 90.0;
    bool front_obstacle =
      psValues[0] > 100.0 ||
      psValues[7] > 100.0;
    // decisions taken
    if (left_obstacle && front_obstacle) {
      turn_right();
    }
    else if (right_obstacle && front_obstacle) {
      turn_left();
    }
    else{
      drive_forward();
    }
  }

  delete robot;
  return 0; //EXIT_SUCCESS
}
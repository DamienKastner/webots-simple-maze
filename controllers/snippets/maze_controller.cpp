#include <webots/Robot.hpp>
#include <webots/DistanceSensor.hpp>
#include <webots/Motor.hpp>

// time in [ms] of a simulation step
#define TIME_STEP 64

#define MAX_SPEED 6.28

// All the webots classes are defined in the "webots" namespace
using namespace webots;


//--------------------------------------------------------------
//                    Variables declaration
//--------------------------------------------------------------
// Create the Robot instance.


// Create the motors instance


// Create the sensors arrays


// Create the whell speed variables



//--------------------------------------------------------------
//                    Utility functions
//--------------------------------------------------------------



//--------------------------------------------------------------
//                    Main function
//--------------------------------------------------------------

int main(int argc, char **argv) {
  // Initialize all the objects needed

  // feedback loop: step simulation until an exit event is received
  while (robot->step(TIME_STEP) != -1) {
    // read sensors outputs

    // detect obstacles

    // decide how to move

  }

  delete robot;
  return 0; //EXIT_SUCCESS
}
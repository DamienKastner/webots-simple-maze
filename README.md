# Webots Simple Maze

In this repository, you will find the files required for the Robot simulation 
tutorial for PCOnline 2020.  
  
The world folder contains the maze arena.  
The controller folder contains a simple controller that is able to solve the 
maze.